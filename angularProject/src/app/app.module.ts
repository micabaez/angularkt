import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { APP_ROUTING } from './app.routes';
import { ButtonModule } from 'primeng/button';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { FooterComponent } from './components/footer/footer.component';
import { BodyComponent } from './components/body/body.component';
import { NoticiasComponent } from './components/noticias/noticias.component';
import { SociosComponent } from './components/socios/socios.component';
import { EntradasComponent } from './components/entradas/entradas.component';
import { ErrorPageComponent } from './components/error-page/error-page.component';
import { SplitterModule } from 'primeng/splitter';
import { CardModule } from 'primeng/card';
import { SpeedDialModule } from 'primeng/speeddial';
import { ToastModule } from 'primeng/toast';
import { DividerModule } from 'primeng/divider';
import { TiendaComponent } from './components/tienda/tienda.component';
import { MenubarModule } from 'primeng/menubar';
import { LoginComponent } from './components/login/login.component';
import { PasswordModule } from 'primeng/password';
import { FormsModule } from '@angular/forms';
import { InputTextModule } from 'primeng/inputtext';
import { PlantelComponent } from './components/plantel/plantel.component';
import { JsonService } from './services/json.service';
import { TestComponent } from './components/test/test.component';
import { HttpClientModule } from '@angular/common/http';
import { TableModule } from 'primeng/table';
import { RippleModule } from 'primeng/ripple';
import { LigasComponent } from './components/ligas/ligas.component';
import { PlantelactualComponent } from './components/plantelactual/plantelactual.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSlideToggleModule } from '@angular/material/slide-toggle'

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    BodyComponent,
    NoticiasComponent,
    SociosComponent,
    EntradasComponent,
    ErrorPageComponent,
    TiendaComponent,
    LoginComponent,
    PlantelComponent,
    TestComponent,
    LigasComponent,
    PlantelactualComponent
  ],
  imports: [
    BrowserModule,
    ButtonModule,
    SplitterModule,
    CardModule,
    SpeedDialModule,
    ToastModule,
    DividerModule,
    MenubarModule,
    PasswordModule,
    FormsModule,
    InputTextModule,
    HttpClientModule,
    TableModule,
    RippleModule,
    APP_ROUTING,
    BrowserAnimationsModule,
    MatSlideToggleModule
  ],
  providers: [
    JsonService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
