import { Component } from '@angular/core';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-tienda',
  templateUrl: './tienda.component.html',
  styleUrls: ['./tienda.component.css']
})
export class TiendaComponent {
  items: MenuItem[] | undefined;

  constructor() {
    this.items = [
      {
        label: 'Indumentaria',
        icon: 'pi pi-fw pi-user',
        items: [
            {
                label: 'Fútbol',
                items: [
                    {
                        label: 'Niños',
                    },
                    {
                        label: 'Adultos',
                    }
                ]
            },
            {
                label: 'Urban',
                items: [
                  {
                      label: 'Niños',
                  },
                  {
                      label: 'Adultos',
                  }
              ]
            },
            // {
            //     separator: true
            // },
            {
                label: 'Accesorios',
            }
        ]
    },
    {
        label: 'Calzado',
        icon: 'pi pi-fw pi-shield',
        items: [
            {
                label: 'Niños',
            },
            {
                label: 'Adultos',
            },
        ]
    },
    {
        label: 'Productos Oficiales',
        icon: 'pi pi-fw pi-gift',
        items: [
            {
                label: 'Bazar',
            },
            {
                label: 'Regalos',
            },
            {
                label: 'Deco',
            }
        ]
    },
    {
        label: 'Cart',
        icon: 'pi pi-fw pi-shopping-cart'
    }
    ];
  }
}
