import { Component, OnInit } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';

@Component({
    selector: 'app-body',
    templateUrl: './body.component.html',
    styleUrls: ['./body.component.css'],
    providers: [MessageService]
})
export class BodyComponent implements OnInit {
    items: MenuItem[] = [];

    constructor(private messageService: MessageService) {}

    ngOnInit() {
        this.items = [
            
            {
                icon: 'pi pi-facebook',
                target: '_blank',
                url: 'https://es-es.facebook.com/riverplateoficial/'
            },
            {
                icon: 'pi pi-instagram',
                target: '_blank',
                url: 'https://www.instagram.com/riverplate/?hl=es'
            },
            {
                icon: 'pi pi-twitter',
                target: '_blank',
                url: 'https://twitter.com/carpoficial?lang=es'
            }
        ];
    }
}

