import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

  export class LoginComponent implements OnInit{
    signupUsers: any[] = []; 

    signUpObj: any = {
      user: "",
      email: "",
      pass:""
    }

    loginObj: any = {
      pass: "",
      user: ""
    }

    // constructor(){}
  
    ngOnInit(): void{
      let localData = localStorage.getItem("signupUsers");
      if (localData != null){
        this.signupUsers = JSON.parse(localData);
      }
    }

    onSignUp(){
      this.signupUsers.push(this.signUpObj);
      localStorage.setItem("signupUsers", JSON.stringify(this.signupUsers));
      this.signUpObj = {
        user:"",
        email: "",
        pass: ""
      }
    }

    onLogIn(){
      let isUserExist = this.signupUsers.find(m => m.user == this.loginObj.user && m.pass == this.loginObj.pass);
      if(isUserExist != undefined){
        // alert("User Login Successfully");
        Swal.fire({
          icon: 'success',
          iconColor: 'red',
          title: 'Inicio de sesión exitoso',
          showConfirmButton: false,
          footer: '<a href=/home>Siga disfrutando del sitio</a>',
          allowOutsideClick: false
        })
      } else{
        //alert("User Login Failed");
        Swal.fire({
          icon: 'error',
          title: 'Inicio de sesión fallido',
          text: 'Revise su nombre de usuario o contraseña.',
          confirmButtonText: 'Reintentar',
          confirmButtonColor: 'red',
        })
      }
    }

 


}
