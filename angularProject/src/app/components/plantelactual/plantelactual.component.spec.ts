import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlantelactualComponent } from './plantelactual.component';

describe('PlantelactualComponent', () => {
  let component: PlantelactualComponent;
  let fixture: ComponentFixture<PlantelactualComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PlantelactualComponent]
    });
    fixture = TestBed.createComponent(PlantelactualComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
