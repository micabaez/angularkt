import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { JsonService } from 'src/app/services/json.service';

@Component({
  selector: 'app-plantelactual',
  templateUrl: './plantelactual.component.html',
  styleUrls: ['./plantelactual.component.css']
})

export class PlantelactualComponent implements OnInit{

  responseFromAPI!: PlantelActualAPI[]

  constructor(private service:JsonService){}

  ngOnInit(): void { 
    this.getPlantelActual();
  }

  getPlantelActual(): void {
    this.service.callToAPI("http://localhost:8082/kt2023").then(response => {
      console.log("row response: ")
      console.log(response)
    this.responseFromAPI = response
      console.log("*****************")
      console.log(this.responseFromAPI)
    }).catch(error => {
      let errorMessage
      if (error instanceof HttpErrorResponse) {
        errorMessage = "Error de HttpErrorResponse: " + error.message
      }else{
        errorMessage = "Error desconocido"
      }
      console.warn(errorMessage + ' => ' + error)
    })
  }

  getBanderas(nacionalidad: string): string{
    let pathBandera: string
    switch(nacionalidad){
      case "Argentina":
      pathBandera = "/assets/images/arg.png"
      break
      case "Venezuela":
      pathBandera = "/assets/images/ven.png"
      break
      case "Colombia":
      pathBandera = "/assets/images/col.webp"
      break
      case "Chile":
      pathBandera = "/assets/images/chi.png"
      break
      case "Uruguay":
      pathBandera = "/assets/images/uru.webp"
      break
      default:
        pathBandera = ""
    }
    return pathBandera
  }
  getPosicion(posicion: string): string{
    let pathPosicion: string
    switch(posicion){
      case "Arquero":
        pathPosicion = "/assets/images/arquero.png"
      break
      case "Defensor":
        pathPosicion = "/assets/images/def.png"
      break
      case "Delantero":
        pathPosicion = "/assets/images/del.png"
      break
      case "Volante":
        pathPosicion = "/assets/images/vol.png"
      break
      default:
        pathPosicion = ""
    }
    return pathPosicion
  }

  }

export interface PlantelActualAPI{
    nacionalidad: string
    posicion: string
		nombre: string
		edad: string
}

