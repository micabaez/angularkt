import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { JsonService } from 'src/app/services/json.service';

@Component({
  selector: 'app-plantel',
  templateUrl: './plantel.component.html',
  styleUrls: ['./plantel.component.css']
})

export class PlantelComponent {

  display = false

  responseFromAPI!: PlantelAPI[]

  constructor(private service:JsonService){}

  getDisplay(){
    this.display = true
  }

  getPlantel2018(): void {
    this.service.callToAPI("http://localhost:8082/kt2018").then(response => {
      console.log("row response: ")
      console.log(response)
    this.responseFromAPI = response
      console.log("*****************")
      console.log(this.responseFromAPI)
    }).catch(error => {
      let errorMessage
      if (error instanceof HttpErrorResponse) {
        errorMessage = "Error de HttpErrorResponse: " + error.message
      }else{
        errorMessage = "Error desconocido"
      }
      console.warn(errorMessage + ' => ' + error)
    })
  }

  getPlantel2015(): void {
    this.service.callToAPI("http://localhost:8082/kt2015").then(response => {
      console.log("row response: ")
      console.log(response)
    this.responseFromAPI = response
      console.log("*****************")
      console.log(this.responseFromAPI)
    }).catch(error => {
      let errorMessage
      if (error instanceof HttpErrorResponse) {
        errorMessage = "Error de HttpErrorResponse: " + error.message
      }else{
        errorMessage = "Error desconocido"
      }
      console.warn(errorMessage + ' => ' + error)
    })
  }

  getPlantel1996(): void {
    this.service.callToAPI("http://localhost:8082/kt1996").then(response => {
      console.log("row response: ")
      console.log(response)
    this.responseFromAPI = response
      console.log("*****************")
      console.log(this.responseFromAPI)
    }).catch(error => {
      let errorMessage
      if (error instanceof HttpErrorResponse) {
        errorMessage = "Error de HttpErrorResponse: " + error.message
      }else{
        errorMessage = "Error desconocido"
      }
      console.warn(errorMessage + ' => ' + error)
    })
  }

  getPlantel1986(): void {
    this.service.callToAPI("http://localhost:8082/kt1986").then(response => {
      console.log("row response: ")
      console.log(response)
    this.responseFromAPI = response
      console.log("*****************")
      console.log(this.responseFromAPI)
    }).catch(error => {
      let errorMessage
      if (error instanceof HttpErrorResponse) {
        errorMessage = "Error de HttpErrorResponse: " + error.message
      }else{
        errorMessage = "Error desconocido"
      }
      console.warn(errorMessage + ' => ' + error)
    })
  }

  getBanderas(nacionalidad: string): string{
    let pathBandera: string
    switch(nacionalidad){
      case "Argentina":
      pathBandera = "/assets/images/arg.png"
      break
      case "Venezuela":
      pathBandera = "/assets/images/ven.png"
      break
      case "Colombia":
      pathBandera = "/assets/images/col.webp"
      break
      case "Chile":
      pathBandera = "/assets/images/chi.png"
      break
      case "Uruguay":
      pathBandera = "/assets/images/uru.webp"
      break
      case "Paraguay":
      pathBandera = "/assets/images/par.png"
      break
      default:
        pathBandera = ""
    }
    return pathBandera
  }

  getPosicion(posicion: string): string{
    let pathPosicion: string
    switch(posicion){
      case "Arquero":
        pathPosicion = "/assets/images/arquero.png"
      break
      case "Defensor":
        pathPosicion = "/assets/images/def.png"
      break
      case "Delantero":
        pathPosicion = "/assets/images/del.png"
      break
      case "Volante":
        pathPosicion = "/assets/images/vol.png"
      break
      default:
        pathPosicion = ""
    }
    return pathPosicion
  }

}

export interface PlantelAPI{
    nacionalidad: string
    posicion: string
		nombre: string
		nacimiento: string
		ciudad: string
}


