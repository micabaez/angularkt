import { RouterModule, Routes } from '@angular/router';
import { BodyComponent } from './components/body/body.component';
import { NoticiasComponent } from './components/noticias/noticias.component';
import { SociosComponent } from './components/socios/socios.component';
import { EntradasComponent } from './components/entradas/entradas.component';
import { ErrorPageComponent } from './components/error-page/error-page.component';
import { TiendaComponent } from './components/tienda/tienda.component';
import { LoginComponent } from './components/login/login.component';
import { PlantelComponent } from './components/plantel/plantel.component';
import { TestComponent } from './components/test/test.component';
import { LigasComponent } from './components/ligas/ligas.component';
import { PlantelactualComponent } from './components/plantelactual/plantelactual.component';

const ROUTES: Routes = [
    { path: 'home', component: BodyComponent },
    { path: 'noticias', component: NoticiasComponent },
    { path: 'socios', component: SociosComponent },
    { path: 'entradas', component: EntradasComponent },
    { path: 'error', component: ErrorPageComponent},
    { path: 'tienda', component: TiendaComponent},
    { path: 'login', component: LoginComponent},
    { path: 'plantel', component: PlantelComponent},
    { path: 'test', component: TestComponent},
    { path: 'ligas', component: LigasComponent},
    { path: 'plantelactual', component: PlantelactualComponent},
    { path: '**', pathMatch:'full', redirectTo: '/error' }
];

export const APP_ROUTING = RouterModule.forRoot(ROUTES);