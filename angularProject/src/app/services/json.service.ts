import { Injectable } from "@angular/core"
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { firstValueFrom } from 'rxjs'

@Injectable()
export class JsonService {
    
    constructor(private _http: HttpClient) {}

    async callToAPI(url: string): Promise<any> {
      try {
        const requestOptions = this.createRequestOptions()
        console.log("Calling to: " + url)
        this.logRequestHeaders(requestOptions.headers)
        return await firstValueFrom(this._http.get(url, requestOptions))
      } catch (error) {
        console.warn('Error al realizar la solicitud GET:', error)
        throw error
      }
    }
    
    private createRequestOptions(): { headers: HttpHeaders } {
      const headers = new HttpHeaders({
        'Content-Type': 'application/json'
      })
    
      return {
        headers: headers
      }
    }
    
    private logRequestHeaders(headers: HttpHeaders): void {
      console.log('Headers:');
      headers.keys().forEach(name => {
        const values = headers.getAll(name);
        if (values) {
          console.log(`${name}: ${values.join(', ')}`);
        } else {
          console.log(`${name}: null`);
        }
      })
    }
}
